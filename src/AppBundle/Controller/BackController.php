<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Wishlist;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/profile")
 */
class BackController extends Controller
{

    /**
     * @Route("/wishlist", name="user_wishlist")
     */
    public function showWishlistAction(Request $request)
    {

        return $this->render('@App/Back/wishlist.html.twig');
    }


}