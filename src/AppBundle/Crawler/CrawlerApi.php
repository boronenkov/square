<?php

namespace AppBundle\Crawler;


use Symfony\Component\HttpKernel\Client;

class CrawlerApi implements MFAProviderInterface
{
    protected $url;
    protected $client;

    public function __construct($client)
    {
        // here we asignate any valid http client to make requests to the api of AppliancesDelivered.ie
        $this->client = $client;
    }

    public function getProducts($url){
        //$products = $this->getData($url);
        //...
        //...
    }


    private function getData($url){
        $data = $this->client->apiRequest();
        //.....
        return $data;
    }

}