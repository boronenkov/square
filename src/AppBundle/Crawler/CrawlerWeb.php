<?php

namespace AppBundle\Crawler;

use Goutte\Client;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Intl\NumberFormatter\NumberFormatter;


class CrawlerWeb implements MFAProviderInterface
{

    public function getProducts($url)
    {
        $products=[];
        $client = new Client();
        $crawler = $client->request('GET', $url);

        if ($client->getResponse()->getStatus() == Response::HTTP_OK){
            $crawler = $crawler->filter('.search-results');
            $products = $crawler->filter('.search-results-product')->each(function ($node) {

                // Check if form or button exists, in order to get the ID of product
                $id = 0;
                if (count($node->filter('.product-description form')) > 0)
                    $id = basename($node->filter('.product-description form')->attr('action'));
                else if (count($node->filter('.product-description a.btn-learn-more'))> 0)
                    $id = basename($node->filter('.product-description a.btn-learn-more')->attr('href'));

                // If we can get the Id, we will add this product to the list
                if ($id > 0){
                    $currency = $currencySymbol = mb_substr($node->filter('.product-description .section-title')->text(),0,1);
                    $price = mb_substr($node->filter('.product-description .section-title')->text(), 0);
                    $fmt = new \NumberFormatter( 'en_IR', \NumberFormatter::CURRENCY );
                    $price = $fmt->parseCurrency($price, $currency);
                     $props = [
                         'id' => $id,
                        'img' => $node->filter('.product-image img')->attr('src'),
                        'price' => $price,
                        'name' => $node->filter('.product-description h4')->text(),
                         'url' => $node->filter('.product-description h4 a')->attr('href'),
                         'currency' => $currency,
                         'currency_symbol' => $currencySymbol
                     ];

                    return $props;
                }else
                    return false;
            });
        }
        return $products;
    }

}