<?php

namespace AppBundle\Crawler;


use AppBundle\Entity\Product;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Goutte\Client;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;


class Updater
{

    protected $provider;
    protected $em;
    protected $serializer;



    public function __construct(MFAProviderInterface $provider, ObjectManager $em, SerializerInterface $serializer)
    {
        $this->provider = $provider;
        $this->em = $em;
        $this->serializer = $serializer;
    }


    /**
     * public method that executes all the proccess after construct.
     *
     */

    public function execute(){
        $categoryRepository = $this->em->getRepository('AppBundle\Entity\Category');
        $categories = $categoryRepository->findAll();
        foreach($categories as $category){

            // TODO get data with pagination
            $products = $this->provider->getProducts($category->getUrl());
            $this->saveData($products, $category);
            $this->purgeUncheckedProducts();
            $this->resetChecks();
        }

    }

    /**
     *  resets the checked flags for the future updates
     *
     */


    private function resetChecks(){
        $products = $this->em->getRepository('AppBundle\Entity\Product')->findAll();
        foreach ($products as $product){
            $product->setChecked(false);
            $this->em->persist($product);
        }
        $this->em->flush();

    }


    /**
     *  delete all products that doesn't exist in the data source but still exist in our DB
     *
     */

    private function purgeUncheckedProducts(){
        $products = $this->em->getRepository('AppBundle\Entity\Product')->findBy(array(
            'checked' => false
        ));
        foreach ($products as $product){
            $this->em->remove($product);
        }
        $this->em->flush();
    }


    /**
     * save new products to the DB and mark the existent as repeated
     *
     */

    private function saveData($products, $category){

        foreach ($products as $product){
            $productBd = $this->em->getRepository(Product::class)->find($product['id']);
            // Check if product doesnt exist in our DB
            if (!$productBd){
                $prod = $this->serializer->denormalize($product, Product::class);
                $prod->setCategory($category);
                $this->em->persist($prod);
            }else{
                $productBd->setChecked(true);
                $this->em->persist($productBd);
            }
            $this->em->flush();
        }
    }


}