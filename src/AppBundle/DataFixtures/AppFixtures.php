<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Category;
use AppBundle\Entity\Friends;
use AppBundle\Entity\User;
use AppBundle\Entity\Wishlist;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AppFixtures extends Fixture implements ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }


    public function load(ObjectManager $manager)
    {

        // Create categories
        $urls = array(
            'small appliances' => 'https://www.appliancesdelivered.ie/search/small-appliances?sort=price_desc',
            'dishwashers' => 'https://www.appliancesdelivered.ie/dishwashers'
        );

        foreach ($urls as $name => $url){
            $cat = new Category();
            $cat->setName($name);
            $cat->setUrl($url);
            $manager->persist($cat);
        }
        $manager->flush();


        // Create users
        $factory = $this->container->get('security.encoder_factory');
        /** @var $manager \FOS\UserBundle\Doctrine\UserManager */
        $userManager = $this->container->get('fos_user.user_manager');
        $users = [];
        for ($i = 0; $i < 2; $i++)
        {
            /** @var $user \AppBundle\Entity\User */
            $user = $userManager->createUser();
            $user->setUsername("user".$i);
            $user->setPlainPassword("user".$i);
            $user->setEmail("user".$i."@mail.com");
            $user->setRoles(array('ROLE_USER'));
            $user->setEnabled(true);
            $encoder = $factory->getEncoder($user);
            $password = $encoder->encodePassword($user->getPlainPassword(), $user->getSalt());
            $user->setPassword($password);
            $userManager->updateUser($user, true);
            $users[$i] = $user;

            // create wishlist
            $wishlist = new Wishlist($user);
            $manager->persist($wishlist);
            $manager->flush();
        }

        // Create friendship
        $friendship = new Friends();
        $friendship->setUserA($users[0]);
        $friendship->setUserB($users[1]);
        $friendship->setEnabled(true);
        $manager->persist($friendship);
        $manager->flush();

    }
}