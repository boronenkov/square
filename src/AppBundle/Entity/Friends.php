<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="friends")
 */
class Friends
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="friends")
     * @ORM\JoinColumn(name="user_a_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $userA;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_b_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $userB;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $enabled;


    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Friends
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set enabled
     *
     * @param bool $enabled
     *
     * @return Friends
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return \bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set userA
     *
     * @param \AppBundle\Entity\User $userA
     *
     * @return Friends
     */
    public function setUserA(\AppBundle\Entity\User $userA = null)
    {
        $this->userA = $userA;

        return $this;
    }

    /**
     * Get userA
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserA()
    {
        return $this->userA;
    }

    /**
     * Set userB
     *
     * @param \AppBundle\Entity\User $userB
     *
     * @return Friends
     */
    public function setUserB(\AppBundle\Entity\User $userB = null)
    {
        $this->userB = $userB;

        return $this;
    }

    /**
     * Get userB
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserB()
    {
        return $this->userB;
    }
}
