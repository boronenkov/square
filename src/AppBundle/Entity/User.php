<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\OneToOne(targetEntity="Wishlist", mappedBy="user", cascade={"persist"})
     *
     */

    protected $wishlist;

    /**
     * @ORM\OneToMany(targetEntity="Friends", mappedBy="userA", cascade={"remove"})
     */
    protected $friends;

    /**
     * Set wishlist
     *
     * @param \AppBundle\Entity\Wishlist $wishlist
     *
     * @return User
     */
    public function setWishlist(\AppBundle\Entity\Wishlist $wishlist = null)
    {
        $this->wishlist = $wishlist;

        return $this;
    }

    /**
     * Get wishlist
     *
     * @return \AppBundle\Entity\Wishlist
     */
    public function getWishlist()
    {
        return $this->wishlist;
    }

    /**
     * Add friend
     *
     * @param \AppBundle\Entity\Friends $friend
     *
     * @return User
     */
    public function addFriend(\AppBundle\Entity\Friends $friend)
    {
        $this->friends[] = $friend;

        return $this;
    }

    /**
     * Remove friend
     *
     * @param \AppBundle\Entity\Friends $friend
     */
    public function removeFriend(\AppBundle\Entity\Friends $friend)
    {
        $this->friends->removeElement($friend);
    }

    /**
     * Get friends
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFriends()
    {
        return $this->friends;
    }
}
