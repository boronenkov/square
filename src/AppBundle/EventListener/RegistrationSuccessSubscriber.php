<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Wishlist;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class RegistrationSuccessSubscriber implements EventSubscriberInterface
{

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;

    }


    public function onRegistrationSuccess(FilterUserResponseEvent $event)
    {
       // Create the wishlist for our user after his successful registration
        $user = $event->getUser();
        $w = new Wishlist($user);
        $user->setWishlist($w);
    }


    public static function getSubscribedEvents()
    {
        return [
            FOSUserEvents::REGISTRATION_COMPLETED => 'onRegistrationSuccess'
        ];
    }
}