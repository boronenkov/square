<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManagerInterface;

class ProductManager
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getProducts()
    {
        $products = $this->em->getRepository('AppBundle\Entity\Product')->findAll();

        return $products;
    }
}