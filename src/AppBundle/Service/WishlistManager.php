<?php

namespace AppBundle\Service;

use AppBundle\Entity\Product;
use AppBundle\Entity\Wishlist;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class WishlistManager
{
    private $em;
    private $tokenStorage;
    private $router;


    public function __construct(EntityManagerInterface $em, TokenStorageInterface $tokenStorage, RouterInterface $router)
    {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
        $this->router = $router;

    }


    public function addProductToWishlist(Wishlist $wishlist, Product $product){

        $response = [];
        if (!$wishlist->getProducts()->contains($product)){
            $wishlist->addProduct($product);
            $this->em->persist($wishlist);
            $this->em->flush();
            $response['code'] = 200;
            $response['body']['message'] = 'Product added to wishlist';
            $response['body']['data'] = [
                'product' => $product->getId(),
                'delete_url' => $this->router->generate('ajax_delete_wishlist_product', ['wishlist' => $wishlist->getId(), 'product' => $product->getId()], Router::ABSOLUTE_PATH),
            ];
        }else{
            $response['code'] = 400;
            $response['body']['message'] = 'Product already exists in this wishlist';
        }
        return $response;
    }

    public function removeProductFromWishlist(Wishlist $wishlist, Product $product){

        $response = [];
        if ($wishlist->getProducts()->contains($product)){
            $wishlist->removeProduct($product);
            $this->em->persist($wishlist);
            $this->em->flush();
            $response['code'] = 200;
            $response['body']['message'] = 'Product removed from wishlist';
            $response['body']['data'] = [
                'product' => $product->getId(),
                'add_url' => $this->router->generate('ajax_add_wishlist_product', ['wishlist' => $wishlist->getId(), 'product' => $product->getId()], Router::ABSOLUTE_PATH),
            ];
        }else{
            $response['code'] = 400;
            $response['body']['message'] = 'The wishlist doesnt contain the product';
        }
        return $response;

    }


}